<?php

namespace MormonCanon;

class Display
{
    private $link;
    private $book;
    private $chapter;
    private $page_title;

    public function __construct(array $getVar, string $dbName, string $mysqlUser, string $mysqlPassword = '', string $dbHost = '')
    {
        $dbHost = '' === $dbHost ? 'localhost' : $dbHost;
        if ('.' === $dbHost) {
            $this->link = new \mysqli($dbHost, $mysqlUser, $mysqlPassword, $dbName, (int) ini_get('mysqli.default_port'), 'mysql');
        } else {
            $this->link = new \mysqli($dbHost, $mysqlUser, $mysqlPassword, $dbName);
        }

        if ($this->link->connect_error) {
            die('Connect Error ('.$this->link->connect_errno.') '
              .$this->link->connect_error);
        }

      
        $this->book = mysqli_real_escape_string($this->link, $getVar['book'] ?? '');
        $this->chapter = mysqli_real_escape_string($this->link, $getVar['chapter'] ?? '');

        if (!$this->is_blank($this->book) && $this->is_blank($this->chapter)) {
            $this->chapter = 1;
        }
    }

    public function build_page(): string
    {
        $bookArray = array('Genesis','Exodus','Leviticus','Numbers','Deuteronomy','Joshua','Judges','Ruth','1 Samuel','2 Samuel','1 Kings','2 Kings','1 Chronicles','2 Chronicles','Ezra','Nehemiah','Esther','Job','Psalms','Proverbs','Ecclesiastes','Song of Solomon','Isaiah','Jeremiah','Lamentations','Ezekiel','Daniel','Hosea','Joel','Amos','Obadiah','Jonah','Micah','Nahum','Habakkuk','Zephaniah','Haggai','Zechariah','Malachi','Matthew','Mark','Luke','John','Acts','Romans','1 Corinthians','2 Corinthians','Galatians','Ephesians','Philippians','Colossians','1 Thessalonians','2 Thessalonians','1 Timothy','2 Timothy','Titus','Philemon','Hebrews','James','1 Peter','2 Peter','1 John','2 John','3 John','Jude','Revelation','1 Nephi','2 Nephi','Jacob','Enos','Jarom','Omni','Words of Mormon','Mosiah','Alma','Helaman','3 Nephi','4 Nephi','Mormon','Ether','Moroni','Doctrine and Covenants','Moses','Abraham','JST Matthew','Joseph Smith History','Articles of Faith');
        if (!$this->is_blank($this->book) && !in_array(str_replace("_", " ", $this->book), $bookArray)) {
            return $this->print_error("404");
        }

        $page_title = "Mormon Canon";

        if (!$this->is_blank($this->book)) {
            $page_title = "" . str_replace("_", " ", $this->book) . ", {$this->chapter}";
        }

        $output = "
          <!DOCTYPE html>
            <html lang='en'>
              <head>
                <title>{$page_title}</title>
                <meta name='viewport' content='width=device-width, initial-scale=1'>
                <meta charset='utf-8'>
                <meta http-equiv='Content-type' content='text/html; charset=utf-8'>
                <meta name='description' content='A resource for studying the canon of the Mormon church.'>
                <meta name='keywords' content='Mormon Canon,Mormon scriptures,Mormon,scriptures,LDS,canon,Latter-Day Saints,Christianity'>
              </head>
              <body style='padding:20px;'>";
              
        if ($this->is_blank($this->book)) {
            $output .= $this->print_scripture_index();
        } elseif ($this->is_blank($this->chapter)) {
            $output .= $this->print_scripture(1);
        } else {
            $output .= $this->print_scripture();
        }
        $output .= "
            </body>
          </html>
          <script src='//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js'></script>
          <script type='text/javascript'> $(document).ready(function(){ $('.selector').change( function(){ window.location.href = this.value; }); }); </script>
          {$this->print_chapter_array()}";
 
        mysqli_close($this->link);
        return $output;
    }
  
    public function print_chapter_array()
    {
      $output = '';
        $asdf = mysqli_query($this->link, "SELECT * FROM lds_scriptures_books");
        if ($asdf !== false) {
            $output .= "<div style='display:none;'>array(";
            while ($row = mysqli_fetch_array($asdf)) {
                $output .= $row['num_chapters'] . ",";
            }
            $output .= ");</div>";
        }
        return $output;
    }

    public function print_error($error)
    {
        return "<h1>{$error}</h1><a href='/'>Return to homepage</a>";
    }

    public function print_scripture_index()
    {
        $get_books_query =
              mysqli_query(
                  $this->link,
                "SELECT * 
                FROM lds_scriptures_books
                INNER JOIN lds_scriptures_volumes
                ON lds_scriptures_books.volume_id = lds_scriptures_volumes.volume_id"
              );
        if (!$get_books_query) {
          return '';
        }

        $last_volume_id = null;
        $is_not_first_iteration = false;

        $output = "<h1 style='font-variant:small-caps;color:#000088;'>Mormon Canon</h1>";

        while ($row = mysqli_fetch_array($get_books_query)) {
            $volume_id = $row['volume_id'];
            $volume_title = $row['volume_title'];
            $book_title = $row['book_title'];

            if ($volume_id !== $last_volume_id) {
                if ($is_not_first_iteration) {
                    $output .= "</ul></div>";
                }
                $output .= "<div style='float:left;width:240px;margin-right:20px;'><h2>{$volume_title}</h2><ul>";
            }

            $is_not_first_iteration = true;
            $output .= "<li><a href='/" . str_replace(" ", "_", $book_title) . "/1'>" . $book_title . "</a></li>";
            $last_volume_id = $volume_id;
        }

        return $output;
    }
  
    public function print_scripture($chapter = null)
    {
        $this_book_abbreviation = $this->book;
        $getBookInfo = mysqli_query($this->link, "SELECT * FROM lds_scriptures_books WHERE( book_title LIKE '$this_book_abbreviation' )");

        if (!$getBookInfo) {
          return '';
        }

        $this_book_id = 0;
        $current_volume_name = '';
        $this_book_name = '';
        $this_book_chapters = 0;
        while ($row = mysqli_fetch_array($getBookInfo)) {
            $this_book_id = $row['book_id'];
            $this_book_chapters = $row['num_chapters'];
            $this_book_name = $row['book_title'];
            $this_book_name_long = $row['book_title_long'];
        }
        $this_chapter = $chapter;

        if (is_null($this_chapter)) {
            $this_chapter = $this->chapter;
        }
    
        $get_scripture_info_query = mysqli_query(
          $this->link,
          "SELECT * 
          FROM lds_scriptures_books 
          INNER JOIN lds_scriptures_volumes ON lds_scriptures_books.volume_id = lds_scriptures_volumes.volume_id
          WHERE book_id = '$this_book_id' 
          LIMIT 1"
        );
        
        if (!$get_scripture_info_query) {
          return '';
        }
      
        while ($row = mysqli_fetch_array($get_scripture_info_query)) {
            $volume_id = $row['volume_id'];
            $book_id = $row['book_id'];
            $number_of_chapters = $row['num_chapters'];
            $current_volume_name = $row['volume_title'];
        }

        $get_books_query = mysqli_query(
          $this->link,
          "SELECT * 
          FROM lds_scriptures_books
          INNER JOIN lds_scriptures_volumes ON lds_scriptures_books.volume_id = lds_scriptures_volumes.volume_id"
        );
        $getChapters = mysqli_query(
          $this->link,
          "SELECT * 
          FROM lds_scriptures_books 
          WHERE book_id = '{$this_book_id}' 
          LIMIT 1"
        );
        $printChapter = mysqli_query(
          $this->link,
          "SELECT * FROM lds_scriptures_verses WHERE( book_id = $this_book_id && chapter = $this_chapter )"
        );

        $last_volume_id = null;
        $notFirstRun = false;

        $output = "
          <div>
            <a href='/'>Mormon Canon</a> &rarr;
            <a href='/'>{$current_volume_name}</a> &rarr;
            <div style='display:inline;'>
              <select class='selector'>
                <optgroup label='Old Testament'>";
        if ($get_books_query) {
            while ($row = mysqli_fetch_array($get_books_query)) {
                $this_volume_id = $row['volume_id'];
                if (($this_volume_id !== $last_volume_id) && $notFirstRun) {
                    $output .= "</optgroup><optgroup label='{$row['volume_title']}'>";
                }
                $notFirstRun=true;
                if ($row['book_id'] == $this_book_id) {
                    $output .= "<option selected='selected'>{$row['book_title']}</option>";
                } else {
                    $book_title_underscored = str_replace(" ", "_", $row['book_title']);
                    $output .= "<option value='/{$book_title_underscored}/1'>{$row['book_title']}</option>";
                }
                $last_volume_id = $this_volume_id;
            }
        }
          
        $output .= "</optgroup></select></div>";
      
        $output .= "<div style='display:inline;'><select class='selector'>";
        if ($getChapters) {
            while ($row = mysqli_fetch_array($getChapters)) {
                for ($i = 1; $i <= $row['num_chapters']; $i++) {
                    if ($i == $this_chapter) {
                        $output .= "<option selected='selected'>{$i}</option>";
                    } else {
                        $book_title_underscored = str_replace(" ", "_", $row['book_title']);
                        $output .= "<option value='/{$book_title_underscored}/{$i}'>{$i}</option>";
                    }
                }
            }
        }
        $output .= "</select></div></div>";
    
        $output .= "<h1>{$this_book_name}, {$this_chapter}</h1>";
    
        $output .= "<div>";
        if ($printChapter) {
            while ($row = mysqli_fetch_array($printChapter)) {
                $output .= "
                  <div>
                    <div style='float:left; margin-right:10px; width:10px; text-align:right; font-size:11px; color:#aaa;'>{$row['verse']}</div>
                    <div style='overflow:auto;'>{$row['verse_scripture']}</div>
                  </div>";
            }
        }
        $output .= "</div>";

        if ($this_chapter > 1 || $this_chapter < $this_book_chapters) {
            $output .= "<div style='margin-top:20px;clear:both;display:table;width:100%;'>";
            if ($this_chapter > 1) {
                $output .= "<a style='float:left;' href='" . ($this_chapter - 1) . "'>&larr; Chapter " . ($this_chapter - 1) . "</a>";
            }
            if ($this_chapter < $this_book_chapters) {
                $output .= "<a style='float:right;' href='" . ($this_chapter + 1) . "'>Chapter " . ($this_chapter + 1) . " &rarr;</a>";
            }
            $output .= "</div>";
        }

        return $output;
    }

    private function is_blank($input)
    {
        if (is_null($input) || $input == "" || $input == " ") {
            return true;
        } else {
            return false;
        }
    }

}
